#!/bin/bash

HANSEG="../hanseg.py -m kaist_bilou.mdl"

test () {
    echo -n "Testing $1..."
    $HANSEG $2 testdata.raw > tmp
    if [[ `diff -q tmp testdata.$3` == "" ]]; then 
      echo "passed."
    else
      echo "failed."
    fi
    rm tmp
}

test "segmentation" '' seg
test "recomposition to Hangul" -H han
test "stemming" -s stem
test "unalignable morpheme removal" -u unalign
