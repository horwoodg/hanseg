#!/usr/bin/python


import re,sys
from hangulcompjamo import *


KO = re.compile(u".*[\u1100-\u11FF\u3130-\u318F\uAC00-\uD7A3].*")


if __name__ == "__main__":

    for line in sys.stdin:
        fields = [f.strip() 
                  for f in line.decode("euckr","ignore").split("\t")]
        if len(fields)==4 and all([f != "" for f in fields]):
            if fields[0]!="*":
                sys.stdout.write("\n")
                pfx = "R"
            else:
                pfx = "S"
            jamo = vowel_decompose(jamo2compjamo(fields[1]))
            if len(jamo)==1:
                labels = ["U"]
            else:
                labels = ["I" for i in range(len(jamo))]
                labels[0] = "B"
                labels[-1] = "L"
            labels = ["%s-%s" % (pfx, labels[i])
                              if KO.match(jamo[0]) else "O" 
                              for i in range(len(labels))]
            tag = fields[2]
            for j, l in zip(jamo,labels):
                sys.stdout.write(("%s\t%s\n" % (j, l)).encode('utf8'))

