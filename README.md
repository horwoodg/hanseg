PYthon HANgul SEGmenter v.0.7.1
===============================

Created: 2013-11-18
Last Modified: 2014-05-14

I. INSTALLATION
---------------

Unpack hanseg.tar.gz to the desired location or run:

    git clone https://horwoodg@bitbucket.org/horwoodg/pyhanseg.git


II. USAGE
---------

Usage: hanseg.py [options] [-|FILE]  

Options:  
    -m MODEL, --model=MODEL  Path to a CRF++ model file. [default: kaist_bilou.mdl]  
    -H, --hangul             Recompose jamo into Hangul.  
    -s, --stem               Remove known suffixes.  
    -u, --unalignable        Strip unalignable morphemes.  
    -h, --help  
    --version  

Examples:  

    cat testdata | ./hanseg.py
    ./hanseg.py testdata


II. BUILDING CRFPP FOR ADDITIONAL DISTROS
-----------------------------------------

Hanseg is dependent on CRF++ libraries. Said libraries are included for several platforms:

    Ubuntu12-64bit
    centos6-64bit
    redhat5-64bit

Inclusion of additional distros requires compilation of CRF++ libraries 
specific to the platform.

1) Run `./hanseg.py` to identify your platform and create a subdirectory.

    > ./hanseg.py
    CRF++ libraries not found. Build `_CRFPP.so` and `libcrfpp.so.0` for 
    your platform and copy to:

      path/to/hanseg/libs/<platform>/
 
    > mkdir path/to/hanseg/libs/<platform>

2) Download and compile CRF++ (http://crfpp.googlecode.com/) libraries and copy
 to `libs`.

    > cd 3rdParty
    > wget "https://crfpp.googlecode.com/files/CRF++-0.58.tar.gz"
    > tar xvzf CRF++-0.58.tar.gz
    > cd CRF++-0.58
    > ./configure
    > make
    > cp .libs/libcrfpp.so.0.0.0 <path/to/hanseg>/libs/<platform>/libcrfpp.so.0

3) Build the `_CRFPP.so` wrapper and copy to `libs`.

    > cd python
    > python setup.py build
    > cp build/lib.<arch-python_version>/_CRFPP.so <path/to/hanseg>/libs/<platform>/

