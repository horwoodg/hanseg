#!/usr/bin/env python
# coding=UTF-8

#################################################################################
#
# Convert hangul jamo encoding to hangul compatibility jamo encoding.
#
# Selcuk Kopru, July 2012.
#
#################################################################################

import sys
import unicodedata
from optparse import OptionParser

jamo_initial_c = {
	0x1100:0x3131,
	0x1101:0x3132,
	0x1102:0x3134,
	0x1103:0x3137,
	0x1104:0x3138,
	0x1105:0x3139,
	0x1106:0x3141,
	0x1107:0x3142,
	0x1108:0x3143,
	0x1109:0x3145,
	0x110a:0x3146,
	0x110b:0x3147,
	0x110c:0x3148,
	0x110d:0x3149,
	0x110e:0x314a,
	0x110f:0x314b,
	0x1110:0x314c,
	0x1111:0x314d,
	0x1112:0x314e,
	0x111a:0x3140,
	0x1121:0x3144
}

jamo_medial_v = {
	0x1160:0x3164,
	0x1161:0x314f,
	0x1162:0x3150,
	0x1163:0x3151,
	0x1164:0x3152,
	0x1165:0x3153,
	0x1166:0x3154,
	0x1167:0x3155,
	0x1168:0x3156,
	0x1169:0x3157,
	0x116a:0x3158,
	0x116b:0x3159,
	0x116c:0x315a,
	0x116d:0x315b,
	0x116e:0x315c,
	0x116f:0x315d,
	0x1170:0x315e,
	0x1171:0x315f,
	0x1172:0x3160,
	0x1173:0x3161,
	0x1174:0x3162,
	0x1175:0x3163
}

jamo_final_c = {
	0x11a8:0x3131,
	0x11a9:0x3132,
	0x11aa:0x3133,
	0x11ab:0x3134,
	0x11ac:0x3135,
	0x11ad:0x3136,
	0x11ae:0x3137,
	0x11af:0x3139,
	0x11b0:0x313a,
	0x11b1:0x313b,
	0x11b2:0x313c,
	0x11b3:0x313d,
	0x11b4:0x313e,
	0x11b5:0x313f,
	0x11b6:0x3140,
	0x11b7:0x3141,
	0x11b8:0x3142,
	0x11b9:0x3144,
	0x11ba:0x3145,
	0x11bb:0x3146,
	0x11bc:0x3147,
	0x11bd:0x3148,
	0x11be:0x314a,
	0x11bf:0x314b,
	0x11c0:0x314c,
	0x11c1:0x314d,
	0x11c2:0x314e
}

final_to_initial = {
	0x11a8:0x1100,
	0x11a9:0x1101,
	0x11ab:0x1102,
	0x11ae:0x1103,
	0x11af:0x1105,
	0x11b6:0x111a,
	0x11b7:0x1106,
	0x11b8:0x1107,
	0x11b9:0x1121,
	0x11ba:0x1109,
	0x11bb:0x110a,
	0x11bc:0x110b,
	0x11bd:0x110c,
	0x11be:0x110e,
	0x11bf:0x110f,
	0x11c0:0x1110,
	0x11c1:0x1111,
	0x11c2:0x1112
}

vowel_decompositions = [
	# y + basic + i
	[0x3152, unichr(0x3163)+unichr(0x314F)+unichr(0x3163)],
	[0x3156, unichr(0x3163)+unichr(0x3153)+unichr(0x3163)],
	# w + basic + i
	[0x3159, unichr(0x3157)+unichr(0x314f)+unichr(0x3163)],
	[0x315e, unichr(0x315c)+unichr(0x3153)+unichr(0x3163)],
	# basic + i
	[0x3150, unichr(0x314f)+unichr(0x3163)],
	[0x3154, unichr(0x3153)+unichr(0x3163)],
	[0x315a, unichr(0x3157)+unichr(0x3163)],
	[0x315f, unichr(0x315c)+unichr(0x3163)],
	[0x3162, unichr(0x3161)+unichr(0x3163)],
	# y + basic
	[0x3151, unichr(0x3163)+unichr(0x314f)],
	[0x3155, unichr(0x3163)+unichr(0x3153)],
	[0x315b, unichr(0x3163)+unichr(0x3157)],
	[0x3160, unichr(0x3163)+unichr(0x315c)],
	# w + basic
	[0x3158, unichr(0x3157)+unichr(0x314f)],
	[0x315d, unichr(0x315c)+unichr(0x3153)]
]

def jamo2compjamo(line_utf8):
    line_norm = unicodedata.normalize('NFKD', line_utf8)
    for i, j in dict(jamo_initial_c.items() + jamo_medial_v.items() + jamo_final_c.items()).iteritems():
        line_norm = line_norm.replace(unichr(i), unichr(j))
    return line_norm

def compjamo2jamo(line_utf8):
    # replace compatibility jamo codes with jamo codes
    for i, j in dict(jamo_medial_v.items() + jamo_final_c.items()).iteritems():
        line_utf8 = line_utf8.replace(unichr(j), unichr(i))

    for i, j in jamo_initial_c.iteritems():
        line_utf8 = line_utf8.replace(unichr(j), unichr(i))

    # replace jamo final consonants which are followed by a medial vowel with jamo initial consonants
    length = len(line_utf8)
    new_line = ''
    for i, c in enumerate(line_utf8):
        if i+1 == length:
            new_line += c
        else:
            if ord(line_utf8[i+1]) in jamo_medial_v and ord(c) in final_to_initial:
                new_line += unichr(final_to_initial[ord(c)])
            else:
                new_line += c

    line_j = unicodedata.normalize('NFKC', new_line)
    return line_j

def vowel_decompose(line_utf8):
    for [i, j] in vowel_decompositions:
        line_utf8 = line_utf8.replace(unichr(i), j)
    return line_utf8

def vowel_compose(line_utf8):
    for [i, j] in vowel_decompositions:
        line_utf8 = line_utf8.replace(j, unichr(i))
    return line_utf8

def debug(line_utf8, label):
    print label
    for item in line_utf8:
        item_ord = ord(item)
        if item_ord > 255:
            print '%04x' % ord(item), item, unicodedata.name(item)
        else:
            print '%04x' % ord(item), item

if __name__ == "__main__":

    usage = '%s' % sys.argv[0]
    CmdlParser = OptionParser(usage = usage)
    CmdlParser.add_option("-r", "--reverse", action = "store_true", dest = "reverse", default = False, help = "Convert hangul compatibility jamo encoding to hangul jamo encoding.")
    CmdlParser.add_option("-d", "--decompose", action = "store_true", dest = "decompose", default = False, help = "Decompose. Apply NFKD normalization.")
    CmdlParser.add_option("-c", "--compose", action = "store_true", dest = "compose", default = False, help = "Compose. Apply NFKC normalization.")
    CmdlParser.add_option("-D", "--debug", action = "store_true", dest = "debug", default = False, help = "Print uniname style debug output.")
    CmdlParser.add_option("-V", "--no-vowel-decomposition", action = "store_true", dest = "novoweld", default = False, help = "Disable vowel decomposition.")
    (options, args) = CmdlParser.parse_args()

    f = sys.stdin.readlines()
    for line in f:
        line_utf8 = unicode(line, 'utf-8')

        if options.debug:
           debug(line_utf8, 'input:')

        if options.decompose:
           line_ret = unicodedata.normalize('NFKD', line_utf8)
        elif options.compose:
           line_ret = unicodedata.normalize('NFKC', line_utf8)
        elif options.reverse:
           line_ret = compjamo2jamo(vowel_compose(line_utf8))
        else:
           # No option speficied.
           # Default action is jamo -> compatibility jamo conversion.
           line_cjamo = jamo2compjamo(line_utf8)
           if options.novoweld:
              line_ret = line_cjamo
           else:
              line_ret = vowel_decompose(line_cjamo)

        if options.debug:
           debug(line_ret, 'output:')

        sys.stdout.write(line_ret.encode('utf-8'))
