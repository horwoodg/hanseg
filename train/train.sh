#!/bin/bash

# OPTIONS

function usage() {
  echo "Usage:" 
  echo "  train.sh [options] <path/to/template> <path/to/corpus>"
  echo
  echo "Options:"
  echo "  -f,--minfeat INT    CRF model feature minimum. (Default: 1)"
  echo "  -t,--textmodel      Write CRF model to text file."
  echo "  -p,--parallel INT   Use INT threads in training. (Default: 1)"
  echo "  -j,--jobpath STR    Path to existing training directory. (Default: None)"
  echo "  -o,--holdout STR    Path to a holdout corpus for testing. (Default: None)"
  echo "  -x,--xval INT       X-validate on INT folds. (Default: None)"
  echo "  -r,--report         Report scores to STDOUT."
  echo "  -h,--help"
  echo
  exit -2
}

export FEATURE_MIN=1
export TEXTMODEL=
export THREADS=1
export JOB=
XVAL=0
HOLDOUT=
REPORT=0

shopt -s extglob
while [ "$1" != "" ]
do
  case $1 in
    ([\#\;]*)         break;;      # ignore comments
    (-f|-*minfeat)    export FEATURE_MIN=$2; shift 2;;
    (-t|-*textmodel)  export TEXTMODEL="-t"; shift;;
    (-p|-*parallel)   export THREADS=$2; shift 2;;
    (-j|-*jobpath)    export JOB=$2; shift 2;;
    (-x|-*xval)       XVAL=$2; shift 2;;
    (-o|-*holdout)    HOLDOUT=$2; shift 2;;
    (-r|-*report)     REPORT=1; shift;;
    (-h|-*help)       shift; usage;;
    (--)              shift; break;;
    (*)               break;;
  esac
done

export TEMPLATE=$1
export CORPUS=$2
export CORPUSNAME=${CORPUS##*/}

if [[ "$TEMPLATE" = '' || "$CORPUS" = '' ]]; then 
  usage
fi

# CONSTANTS

export HOME=$(cd `dirname $0` && pwd)
export DATA=$HOME/data
export WORK=$HOME/work
export MODELS=$HOME/../models

export CRFLEARN=$HOME/3rdParty/CRF++-0.58/crf_learn
export CRFTEST=$HOME/3rdParty/CRF++-0.58/crf_test

ID=$RANDOM
if [[ "$JOB" = '' ]]; then JOB=${WORK}/j.${ID}.${TEMPLATE##*/}_f${FEATURE_MIN}; fi
export MODEL=${JOB##*.}.mdl

# FUNCTIONS

function findfile() {
  if [ ! -e $1 ]; then
    echo "\"$1\" not found. Exiting."
    exit 
  fi
}

function testapp() {
  helptxt=`$1 -h`
  if [[ "$helptxt" == '' ]]; then
    echo "\"$1\" not installed. Exiting."
    exit 
  fi
}

function partition_corpus() {
  folds=$1
  corpus_size=`cat $CORPUS | wc -l`
  fold_size=$(( $corpus_size / $folds ))
  for (( fold=0; fold < $folds; fold++ )); do
    fold_start=$(($fold * $fold_size))
    fold_stop=$(($fold_start + $fold_size))
    head -$(($fold_start + $fold_size)) $CORPUS | tail -$fold_size > $JOB/${CORPUSNAME}.test$fold
    head -$fold_start $CORPUS > $JOB/${CORPUSNAME}.train$fold
    tail -$(($corpus_size - $fold_stop)) $CORPUS >> $JOB/${CORPUSNAME}.train$fold
  done
}

function train_model() {
  fold=$1
  if [ -e $TEMPLATE ]; then
    $CRFLEARN $TEXTMODEL -f $FEATURE_MIN -p $THREADS $TEMPLATE $JOB/$CORPUSNAME.train$fold $JOB/$MODEL$fold > $JOB/train.log$fold
  else
    echo "Template not found. Exiting."
    exit -2
  fi
}

function test_model() {
  fold=$1
  if [ -e $JOB/train.log$fold ]; then
    $CRFTEST -m $JOB/$MODEL$fold $JOB/$CORPUSNAME.test$fold > $JOB/test.log$fold
  else
    echo "Training data not found. Exiting" 
    exit -2
  fi
}

function hmean(){
  awk "BEGIN {m=(2*$1*$2)/($1+$2);"' printf "%.3f", m}'
}

function score(){
  # $1 = pattern in the form of <pred>\t<actual>
  # $2 = test log file
  ptn=$1
  total=`cat $2 | grep -P "$ptn" | wc -l`
  cat $2 | awk -F"\t" -v"total=$total" "/$ptn/"'{ if ($(NF-1)==$(NF)) { ++sum } } 
                  END {avg = sum/total; printf "%.3f [%d]", avg, total}'
}

function report_scores() {
  logs=`ls $JOB/test.log* | wc -l`
  if [[ $logs > 0 ]]; then
    cat $JOB/test.log* > $JOB/test.tmp
    echo "Scoring $MODEL."
    echo "Overall: `score "[^\t]+\t[^\t]+$" $JOB/test.tmp`" > $JOB/scores.log
    tags=( $(cut -f3,4 $JOB/test.tmp | sed "s/\t/\n/g" | grep -P "^[A-Z-]+$" | sort -u ) )
    echo "By tag:" >> $JOB/scores.log
    for tag in ${tags[@]}; do
      R=`score "$tag\t[^\t]+$" $JOB/test.tmp`
      P=`score "[^\t]+\t$tag$" $JOB/test.tmp`
      F=`hmean ${R%% *} ${P%% *}`
      echo "  $tag: P=$P, R=$R, F=$F"  >> $JOB/scores.log
    done
    cat $JOB/scores.log
    #rm $JOB/test.tmp
  else
    echo "Test log not found. Exiting."
    exit -2
  fi
  echo
}

export -f hmean
export -f score

# MAIN

# Sanity checks
testapp $CRFTRAIN
findfile $CORPUS
findfile $TEMPLATE
if [[ $HOLDOUT != "" ]]; then 
  findfile $HOLDOUT
fi

# Train
mkdir -p $JOB

if [[ $XVAL != 0 ]]; then
  partition_corpus $XVAL
  for (( fold=0; fold < $XVAL; fold++ )); do
    train_model $fold
    test_model $fold
  done
else
  ln -s "$(readlink -e $CORPUS)" "$JOB/$CORPUSNAME.train"
  train_model 
  cp $JOB/${MODEL} ${MODELS}/$CORPUSNAME.$MODEL
  if [[ $HOLDOUT ]]; then
    ln -s "$(readlink -e $HOLDOUT)" "$JOB/$CORPUSNAME.test"
    test_model  
  fi
fi

if [[ $REPORT != 0 ]]; then 
  report_scores
fi
