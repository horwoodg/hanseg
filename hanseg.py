#!/usr/bin/python

"""
Morphologically segment Korean eojeol into component morphemes.

Usage: HanSeg.py [options] [-|FILE]

Options:
    -m MODEL, --model=MODEL  Path to a CRF++ model file. 
                                [default: kaist_bilou.mdl]
    -H, --hangul             Recompose jamo into Hangul.
    -s, --stem               Remove known suffixes.
    -u, --unalignable        Strip unalignable morphemes.     
    -h, --help
    --version

"""

__authors__   = "Graham Horwood"
__contact__   = "graham.horwood@gmail.com" 
__license__   = "GPL"
__version__   = "0.7.3"


import os, sys, re
from glob import glob
from platform import dist, architecture
from docopt import docopt
from hangulcompjamo import *
from unalignable import *

# The following lines are necessary to import crf++ libraries pre-compiled for
# specific distros.
SCRIPTS = os.path.dirname(__file__)
platforms = [g.split("/")[-1] 
             for g in glob(SCRIPTS+"/libs/*")]
PLATFORM = ''.join(dist()).split('.')[0]+'-'+architecture()[0]
if PLATFORM not in platforms:
    print "CRF++ libraries not found. Build _CRFPP.so and libcrfpp.so.0 \n" + \
          "for your platform and copy to:\n\n" + \
          "  path/to/hanseg/libs/%s/\n" % PLATFORM
    sys.exit()
try:
    import CRFPP
except:
    from ctypes import cdll
    from imp import load_dynamic
    cdll.LoadLibrary('%s/libs/%s/libcrfpp.so' % (SCRIPTS,PLATFORM))
    load_dynamic('_CRFPP', '%s/libs/%s/_CRFPP.so' % (SCRIPTS,PLATFORM))
    import CRFPP


PUNCTUATION = '!"#$%&\'(*+,-./:;<=>?@[\\]^`{|}~'
RE_PUNC = (re.compile(u"(["+PUNCTUATION+"])([\w])", re.U),
           re.compile(u"([\w])(["+PUNCTUATION+")])", re.U))



def strip(text):
    """Removes leading and trailing whitespace, and reduces spans of whitespace
    to a single character."""
    return ' '.join(text.split())


def depunct(text):
    """Inserts a marker between any punctuation mark an an adjacent
    non-punctuation mark, e.g., '(abc)' becomes '( \ufffd abc \ufffd )'. The markers
    serve to indicate where punctuation should be detokenized after words have
    been segmented."""
    for ptn in RE_PUNC:
        text = ptn.sub(u"\\1 \ufffd \\2",text)
    return text


def repunct(text):
    """Removes punctuation marker character '\ufffd' and surrounding whitespace.
    Normalizes any remaining whitespace."""
    return strip(text.replace(u" \ufffd ",""))


def remove_unalignable(text):
    """Removes English-unalignable suffixes from the output."""
    #return ' '.join([w for w in text.split() if not w in UNALIGNABLE])
    return ' '.join([w for w in text.split() if not TAGGED_UNALIGNABLE.match(w)])


def stem(text):
    """Removes known Korean suffixes from the output."""
    return ' '.join([w.replace("_","") for w in text.split() if w not in SUFFIXES])


def insert_boundary_marker(t):
    """Prepends '_' to all suffixes."""
    return " _"+t[0] if t[2]!=0 and (t[1].endswith("B") or t[1].endswith("U")) else t[0]


class Segmenter(CRFPP.Tagger):
    def segment(self, text):
        """Processes each token of the input text. When the token contains any
        Hangul Jamo characters, it is passed to the CRF segmentation model.
        Otherwise, it is returned to the output unmodified."""
        result = []
        for word in text.split():
            if any(w.isalpha() for w in word):
                self.clear()
                jamo = vowel_decompose(jamo2compjamo(word))
                # Convert to Jamo and decompose vowels
                for j in jamo:
                    self.add(j.encode('utf8'))
                # Classify characters
                self.parse()
                chr_tags = [(self.x(i,0), self.y2(i), i) 
                            for i in range(self.size())]
                # Split words at S-B and S-U tags
                tagged = ''.join(map(insert_boundary_marker, chr_tags))
                result += [tagged.decode('utf8')]
            else:
                result += [word]
        return ' '.join(result)


def main():
    args = docopt(__doc__, 
            version='HanSeg %s; CRFPP v.%s' % (__version__, CRFPP.VERSION))
    if args['FILE']:
        conn = open(args['FILE'],'r')
    else:
        conn = sys.stdin
    segmenter = Segmenter("-m %s/models/%s" % (SCRIPTS, args['--model']))
    while True:
        line = conn.readline()
        if not line: break
        line = line.strip().decode('utf8','ignore')
        try:
            result = segmenter.segment(depunct(line))
            if args['--stem']:
                result = stem(result)
            if args['--unalignable']:
                result = remove_unalignable(result)
            out = repunct(result)
            if args['--hangul']:
                out = compjamo2jamo(vowel_compose(out))
        except:
            out = line
        sys.stdout.write(out.encode('utf8')+"\n")
        sys.stdout.flush()


if __name__ == "__main__":
    sys.exit(main())
