#!/usr/bin/python


import re,sys
from hangulcompjamo import *


KO = re.compile(u".*[\u1100-\u11FF\u3130-\u318F\uAC00-\uD7A3].*")
TOPIC = re.compile("__[0-9][0-9]")


def write(c, t, l):
    sys.stdout.write(("%s\t%s\t%s\n" % (c,t,l)).encode('utf8'))


if __name__ == "__main__":

    dups = set()

    for line in sys.stdin:
        fields = [f.strip() 
                  for f in line.decode("utf8","ignore").split("\t")]
        if (len(fields)==3) and \
                (fields[1] not in dups) and \
                (all([f != "" for f in fields])):
            jamo = vowel_decompose(jamo2compjamo(fields[2]))
            morphemes = [w.rsplit("/",1) for w in jamo.split(" + ")]
            for i, (morpheme, tag) in enumerate(morphemes):
                morpheme = TOPIC.sub("",morpheme)
                tag = tag.lower()
                for j, c in enumerate(list(morpheme)):
                    if not KO.match(c):
                        write(c,tag,"O")
                    elif len(morpheme)==1:
                        write(c,tag,"U")
                    elif j==0:
                        write(c,tag,"B")
                    elif j==(len(morpheme)-1):
                        write(c,tag,"L")
                    else:
                        write(c,tag,"I")
            if i==(len(morphemes)-1):
                sys.stdout.write("\n")
            dups.add(fields[1])
